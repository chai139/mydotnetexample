﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dijkstra.Lib
{
    public class Graph
    {
        public List<Vertice> Vertices { get; set; }

        public Graph()
        {
            Vertices = new List<Vertice>();
        }

        public bool AddVertice(string name)
        {
            return AddVertice(name, "", 0);
        }

        public bool AddVertice(string name, string neighbor, int edge)
        {
            bool result = true;
            try
            {
                
                var node = Vertices.FirstOrDefault(n => n.Name == name);


                Vertices.Remove(node);


                if (node == null)
                {
                    node = new Vertice { Name = name };
                }

                if (!String.IsNullOrEmpty(neighbor) && name.ToUpper().Trim() != neighbor.ToUpper().Trim())
                {
                    var nodeNeighbor = Vertices.Where(n => n.Name == neighbor).FirstOrDefault();
                    Vertices.Remove(nodeNeighbor);
                    node = AddNeighbor(node, neighbor, edge);

                    if (nodeNeighbor == null)
                    {
                        nodeNeighbor = new Vertice { Name = neighbor };
                    }
                    nodeNeighbor = AddNeighbor(nodeNeighbor, name, edge);
                    Vertices.Add(nodeNeighbor);
                }
                Vertices.Add(node);
                result = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public Vertice AddNeighbor(Vertice node, string neighbor, int edge)
        {
            Vertice vertice = node;
            if (edge < 1)
            {
                throw new Exception("invalid edge");
            }

            if (vertice.Neighbor.Where(n => n.Name == neighbor).Count() <= 0)
            {
                node.Neighbor.Add(new Vertex { Name = neighbor, Edges = edge });
            }
            else
            {
                throw new Exception("duplicate neighbor");
            }
            return vertice;
        }
    }

    public class Vertice
    {
        public string Name { get; set; }
        public List<Vertex> Neighbor { get; set; }
        public string Previous { get; set; }
        public int Distance { get; set; }
        public Vertice()
        {
            Name = "";
            Neighbor = new List<Vertex>();
        }
    }

    public class Vertex
    {
        public string Name { get; set; }
        public int Edges { get; set; }

        public Vertex()
        {
            Name = "";
            Edges = int.MaxValue;
        }
    }
}
