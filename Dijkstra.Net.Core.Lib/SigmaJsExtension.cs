﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dijkstra.Lib
{
    public class SigmaJsExtension
    {
        public SigmaJs ParseGraph(Graph graph)
        {
            SigmaJs data = new SigmaJs();
            data.Nodes = new List<Node>();
            data.Edges = new List<Edge>();
            Random random = new Random();

            foreach (var node in graph.Vertices)
            {
                data.Nodes.Add(new Node
                {
                    id = node.Name,
                    label = node.Name,
                    x = random.Next(),
                    y = random.Next(),
                    color = "#617db4"
                });

                foreach (var neighbor in node.Neighbor)
                {
                    string source = node.Name;
                    string target = neighbor.Name;

                    var edge = data.Edges.Where(s => (s.source == source && s.target == target)
                        || (s.source == target && s.target == source)
                    ).FirstOrDefault();

                    if (edge == null)
                    {
                        data.Edges.Add(new Edge
                        {
                            id = data.Edges.Count.ToString(),
                            source = source,
                            target = target
                        });
                    }
                }
            }

            return data;
        }

    }

    public class SigmaJs
    {
        public List<Node> Nodes { get; set; }
        public List<Edge> Edges { get; set; }
    }

    public class Node
    {
        public string id { get; set; }

        public string label { get; set; }

        public string color { get; set; }

        public int x { get; set; }

        public int y { get; set; }
    }

    public class Edge
    {
        public string id { get; set; }

        public string source { get; set; }

        public string target { get; set; }
    }
}
