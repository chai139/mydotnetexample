﻿using System;
using Dijkstra.Lib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dijkstra.UnitTest
{
    [TestClass]
    public class DijkstraTest
    {
        [TestMethod]
        public void Dijkstra_DataSourceIsNull_InvalidDataSourceError()
        {
            string msg = "";
            try
            {
                Graph graph = null;
                DijkstraLib dijkstra = new DijkstraLib();
                dijkstra.DataSource = graph;
                var result = dijkstra.ShortestPath("A", "B");
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            finally
            {
                Assert.AreEqual("invalid data source", msg);
            }

        }

        [TestMethod]
        public void Dijkstra_TwoNode_ShortestPath()
        {
            Graph graph = new Graph();
            graph.AddVertice("A");
            graph.AddVertice("B");
            graph.AddVertice("A", "B", 1);

            DijkstraLib dijkstra = new DijkstraLib();
            dijkstra.DataSource = graph;
            var result = dijkstra.ShortestPath("A", "B");
            Assert.AreEqual(1, result.Count, "invalid result");
            Assert.AreEqual("A", result["B"].Name, "invalid result");

        }

        [TestMethod]
        public void Dijkstra_OriginNull_InvalidOriginError()
        {
            string msg = "";
            try
            {
                Graph graph = new Graph();
                graph.AddVertice("A");
                graph.AddVertice("B");
                graph.AddVertice("A", "B", 1);

                DijkstraLib dijkstra = new DijkstraLib();
                dijkstra.DataSource = graph;
                var result = dijkstra.ShortestPath(null, "B");
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            finally
            {
                Assert.AreEqual("invalid origin", msg);
            }
        }

        [TestMethod]
        public void Dijkstra_DestinationNull_InvalidDestinationError()
        {
            string msg = "";
            try
            {
                Graph graph = new Graph();
                graph.AddVertice("A");
                graph.AddVertice("B");
                graph.AddVertice("A", "B", 1);

                DijkstraLib dijkstra = new DijkstraLib();
                dijkstra.DataSource = graph;
                var result = dijkstra.ShortestPath("A", null);
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            finally
            {
                Assert.AreEqual("invalid destination", msg);
            }
        }

        [TestMethod]
        public void Dijkstra_OriginNotExists_OriginNotExistsError()
        {
            string msg = "";
            try
            {
                Graph graph = new Graph();
                graph.AddVertice("A");
                graph.AddVertice("B");
                graph.AddVertice("A", "B", 1);

                DijkstraLib dijkstra = new DijkstraLib();
                dijkstra.DataSource = graph;
                var result = dijkstra.ShortestPath("C", "B");
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            finally
            {
                Assert.AreEqual("origin not exists", msg);
            }
        }

        [TestMethod]
        public void Dijkstra_DestinationNotExists_DestinationNotExistsError()
        {
            string msg = "";
            try
            {
                Graph graph = new Graph();
                graph.AddVertice("A");
                graph.AddVertice("B");
                graph.AddVertice("A", "B", 1);

                DijkstraLib dijkstra = new DijkstraLib();
                dijkstra.DataSource = graph;
                var result = dijkstra.ShortestPath("B", "C");
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            finally
            {
                Assert.AreEqual("destination not exists", msg);
            }

        }


        [TestMethod]
        public void Dijkstra_ThreeWay_ShortestPath()
        {
            Graph graph = new Graph();
            graph.AddVertice("A", "B", 5);
            graph.AddVertice("A", "C", 2);
            graph.AddVertice("B", "C", 1);

            DijkstraLib dijkstra = new DijkstraLib();
            dijkstra.DataSource = graph;
            var result = dijkstra.ShortestPath("A", "C");

            Assert.AreEqual(1, result.Count, "invalid result");
            Assert.AreEqual("A", result["C"].Name, "invalid result");

            result = dijkstra.ShortestPath("A", "B");

            Assert.AreEqual(2, result.Count, "invalid result");
            Assert.AreEqual("C", result["B"].Name, "invalid result");
            Assert.AreEqual("A", result["C"].Name, "invalid result");

        }

        [TestMethod]
        public void Dijkstra_SimpleNode_ShortestPath()
        {
            Graph graph = new Graph();
            graph.AddVertice("A", "B", 6);
            graph.AddVertice("A", "D", 1);
            graph.AddVertice("B", "C", 5);
            graph.AddVertice("B", "D", 2);
            graph.AddVertice("B", "E", 2);
            graph.AddVertice("C", "E", 5);
            graph.AddVertice("D", "E", 1);

            DijkstraLib dijkstra = new DijkstraLib();
            dijkstra.DataSource = graph;
            var result = dijkstra.ShortestPath("A", "C");

            Assert.AreEqual(3, result.Count, "invalid result");
            Assert.AreEqual("E", result["C"].Name, "invalid result");
            Assert.AreEqual("D", result["E"].Name, "invalid result");
            Assert.AreEqual("A", result["D"].Name, "invalid result");
        }

        [TestMethod]
        public void Dijkstra_HomeToSchool_ShortestPath()
        {
            Graph graph = new Graph();
            graph.AddVertice("Home", "A", 3);
            graph.AddVertice("Home", "B", 2);
            graph.AddVertice("Home", "C", 5);
            graph.AddVertice("A", "D", 3);
            graph.AddVertice("B", "D", 1);
            graph.AddVertice("B", "E", 6);
            graph.AddVertice("C", "E", 2);
            graph.AddVertice("D", "F", 4);
            graph.AddVertice("E", "School", 4);
            graph.AddVertice("F", "School", 2);


            DijkstraLib dijkstra = new DijkstraLib();
            dijkstra.DataSource = graph;
            var result = dijkstra.ShortestPath("Home", "School");

            Assert.AreEqual(4, result.Count, "invalid result");
            Assert.AreEqual("F", result["School"].Name, "invalid result");
            Assert.AreEqual("D", result["F"].Name, "invalid result");
            Assert.AreEqual("B", result["D"].Name, "invalid result");
            Assert.AreEqual("Home", result["B"].Name, "invalid result");
        }
    }
}
